import Vue from 'vue';
import Router from 'vue-router';
import store from '../context/store';

import SignIn from '../views/SignIn'
import SignUp from '../views/SignUp'
import Home from '../views/Home' 

Vue.use(Router);

const routes = [
    {
        name: "signin",
        path: "/signin",
        component: SignIn
    },
    {
        name: "signup",
        path: "/signup",
        component: SignUp
    },
    {
        name: "home",
        path: "/",
        component: Home,
        meta: {
            requiresAuth: true
        }
    }
]

const router = new Router({
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) { 
        if (store.getters.isAuth) {
            next()
            return
        }
        next('/signin')
        
    } else { 
        next()
    }
})

export default router;