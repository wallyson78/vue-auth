import Vue from 'vue';
import Vuex from 'vuex';
import router from '../../router';
import api from '../../service/api';


Vue.use(Vuex);

const lsUser = '@vue-auth:user'
const lsToken = '@vue-auth:token'

const authStore = new Vuex.Store({

    state:{
        user: null,
        token: null, 
        logged: false,
    }, 
    getters:{
        isAuth: (state) => {  
            if(!state.logged){
                let _user = window.localStorage.getItem(lsUser);
                let _token = window.localStorage.getItem(lsToken);
    
                if( _user && _token ){
                    console.log('Iniciou Autorizado!')
                    state.user = JSON.parse(_user); 
                    state.token = _token;
                    return true;   
                }
                else{
                    console.log('Iniciou sem autorizacao!') 
                    return false;    
                }
            }
            else{
                return true;
            }
            
        },
        isLoading: state => !!state.loading,
    },
    mutations: {
        signin (state, user, token ){
            state.user = user;
            state.token = token;
            state.logged = true;            
        },
        signout (state){
            state.user = null;
            state.token = null;
            state.logged = false;
        }
    },
    actions:{ 
        async signin (context, {username, password, remember} ){ 
            
            let resAuth = await api.post('/auth/login', {
                username, password
            }).catch( () => { 
                throw "Usuário ou senha invalidos!"
            }); 
            if( resAuth.data.auth === true ){
                api.defaults.headers.common['x-access-token'] = resAuth.data.token

                let resUser = await api.get(`/user/${username}`).catch( e => {
                    throw e.message
                });
                let _user = resUser.data[0]

                context.commit('signin',_user, resAuth.data.token) 

                if(remember){
                    window.localStorage.setItem(lsUser, JSON.stringify(_user));
                    window.localStorage.setItem(lsToken, resAuth.data.token); 
                }                    
                router.push("/")                
            }else{
                throw "Usuário não autorizado."
            } 
        },
        signout (context){
            window.localStorage.clear();
            context.commit('signout');
            router.replace('/signin');
        },
        async signup (context,{name, username, email, password}){
            try{
                await api.post("/auth/signup",{name, username, email, password})
                .catch(error =>{
                    throw error.message
                })
                router.replace('/signin');
            }catch(error){
                throw error.messages
            }
        }
    }
})

export default authStore;